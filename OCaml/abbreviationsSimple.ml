open String

let table_as_string =
  "add 1 alter 3  backup 2  bottom 1  Cappend 2  change 1 \
   Schange  Cinsert 2  Clast 3 compress 4 copy 2 count 3 \
   Coverlay 3 cursor 3  delete 3 Cdelete 2  down 1  duplicate 3 \
   xEdit 1 expand 3 extract 3  find 1 Nfind 2 Nfindup 6 NfUP 3 \
   Cfind 2 findUP 3 fUP 2 forward 2  get  help 1 hexType 4 \
   input 1 powerInput 3  join 1 split 2 spltJOIN load locate 1 \
   Clocate 2 lowerCase 3 upperCase 3 Lprefix 2  macro  merge 2 \
   modify 3 move 2 msg  next 1 overlay 1 parse preserve 4 purge 3 \
   put putD query 1 quit  read recover 3 refresh renum 3 repeat 3 \
   replace 1 Creplace 2 reset 3 restore 4 rgtLEFT right 2 left 2 \
   save  set  shift 2  si  sort  sos  stack 3 status 4 top \
   transfer 3  type 1  up 1"

module Entry = struct
  type t = { word : string ; min : int }
  let compare e1 e2 =
    let n1 = length e1.word in
    if n1 < e2.min || n1 > length e2.word then
      compare e1.word e2.word
    else
      compare (sub e1.word 0 n1) (sub e2.word 0 n1)
end

module Table = Set.Make(Entry)

let clean_strings strs =
  List.filter (fun w -> w <> "" && w <> " ") strs

let rec split = function
  | []      -> Table.empty

  | [w]     ->
     Table.singleton { word=w ; min=length w }

  | w::x::t ->
     try
       let m = int_of_string x in
       Table.add { word=uppercase_ascii w ; min=m } (split t)
     with Failure _ ->
       let m = length w in
       Table.add { word=uppercase_ascii w ; min=m } (split (x::t))

let make_table table_as_string =
  split_on_char ' ' table_as_string
  |> clean_strings
  |> split

let abbrev (table:Table.t) (w:string) : string =
  let w = uppercase_ascii w in
  try
    let e = Table.find { word=w ; min=length w } table in e.word
  with Not_found -> "*error*"

let rec check (table:Table.t) (inputs:string list) : unit =
  List.map (abbrev table) inputs
  |> List.iter (Printf.printf "%s ");
  print_newline ()
      
let main =
  let table = make_table table_as_string in
  let inputs = ["riG";"rePEAT";"copies";"put";"mo";"rest";"types";"fup.";"6";"poweRin"] in
  check table inputs;
  exit 0
